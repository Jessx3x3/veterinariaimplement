const request = require('supertest')
const app = require('../src/index.js')

//Testing - Obtener todos los usuarios

it('Respuesta con todos los usuarios json', done => {
    request(app)
        .get('/users')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
})

//Testing - Obtener User Existente

describe("/GET /users/:id", () => {
    it('Respuesta con UN usuario json', done => {
        request(app)
            .get('/users/5f4de28fa4074e19e0f4a78c')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    })
})

//Testing - Eliminar User Existente

describe("/DELETE /users/:id", () => {
    it('Respuesta con UN usuario json', done => {
        request(app)
            .del('/users/5f7d56c34d7df535c81d784f')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    })
})


//Testing - CREAR usuario existente

describe("POST /users/register", () => {
    it('Crear un usuario, Responder con 200 creado', done => {
        const test = {
            first_name: 'Elmer',
            last_name: 'Figueroa',
            email: 'chayanne@vet.cl',
            password: 'veterinaria',
            create: '2020-09-01T05:56:31.844Z'
        }
        request(app)
            .post('/users/register')
            .send(test)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if(err) return done(err);
                done();
            })
    })
})
