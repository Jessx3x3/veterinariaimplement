const request = require('supertest')
const app = require('../src/index.js')

//Testing - Obtener todos los articulos

it('Respuesta con todos los articulos json', done => {
    request(app)
        .get('/articulos')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
})

//Testing - obtener UN artículo existente

describe("/GET /articulos/:id", () => {
    it('Respuesta con UN articulo json', done => {
        request(app)
            .get('/articulos/5f4dc255dade5c206882b0a5')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    })
})

//Testing - CREAR artículo existente

describe("POST /articulos/", () => {
    it('Crear un artículo, Responder con 201 creado', done => {
        const test = {
            titulo: '¿Cuándo llevar a tu mascota al veterinario?',
            subtitulo: 'Aquí todas las recomendaciones',
            imgUrl: 'https://farmacia-veterinaria.com.es/wp-content/uploads/img-blog-1.jpg',
            contenido: 'CONTENIDO DE PRUEBA'
        }
        request(app)
            .post('/articulos')
            .send(test)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if(err) return done(err);
                done();
            })
    })
})
