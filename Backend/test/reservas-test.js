const request = require('supertest')
const app = require('../src/index.js')

//Testing - Obtener todos las reservas

it('Respuesta con todos los reservas json', done => {
    request(app)
        .get('/reservas')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
})

//Testing - Obtener Reserva Existente

describe("/GET /reservas/:id", () => {
    it('Respuesta con UNA reserva json', done => {
        request(app)
            .get('/reservas/5f508cc401018d3390cff8fd')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    })
})