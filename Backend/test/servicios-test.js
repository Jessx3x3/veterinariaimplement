const request = require('supertest')
const app = require('../src/index.js')

//Testing - Obtener todos los servicios

it('Respuesta con todas las servicios json', done => {
    request(app)
        .get('/servicios')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
})

//Testing - Obtener Servicio Existente

describe("/GET /servicios/:id", () => {
    it('Respuesta con UN servicio json', done => {
        request(app)
            .get('/servicios/5f56f59d3815e32af4683d52')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    })
})