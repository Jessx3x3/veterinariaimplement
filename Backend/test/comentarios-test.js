const request = require('supertest')
const app = require('../src/index.js')

//Testing - Obtener todos los comentarios

it('Respuesta con todos los comentarios json', done => {
    request(app)
        .get('/comentarios')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
})

//Testing - Obtener Comentario Existente

describe("/GET /comentarios/:id", () => {
    it('Respuesta con UN comentario json', done => {
        request(app)
            .get('/comentarios/5f7251f8c047cd2828cbeb34')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    })
})