const express = require('express');
const router = express.Router();

const Reserva = require('../models/Reserva');

/**
 * @async
 * Función para obtener todos los reserva mediante petición GET
 */
router.get('/', async (req, res) => {
    const reserva = await Reserva.find();
    res.json(reserva);
})

/**
 * @async
 * Función para crear reserva mediante petición POST, toma el campos necesarios y lo crea. 
 */
router.post('/', async (req, res) => {
    const reserva = new Reserva(req.body);
    await reserva.save();
    res.json({
        status: 'Reserva Creada'
    });
})

/**
 * @async
 * Función para eliminar un reserva. Mediante petición DELETE más el ID del reserva. Obtiene el reserva y lo elimina. 
 */
router.delete('/:id', async (req, res) => {
    await Reserva.findByIdAndRemove(req.params.id);
    res.json({
        status: 'Reserva Eliminada'
    });
});

/**
 * @async
 * Función para obtener reserva en específico mediante petición GET más el ID del objeto.
 */
router.get('/:id', async (req, res) => {
    const reserva = await Reserva.findById(req.params.id);
    res.json(reserva);
});

module.exports = router;