const express = require('express');
const router = express.Router();

const Servicio = require('../models/Servicio');

/**
 * @async
 * Función para obtener todos los servicios mediante petición GET.
 */
router.get('/', async (req, res) => {
    const servicio = await Servicio.find();
    res.json(servicio);
});

/**
 * @async
 * Función para crear servicios mediante petición POST, toma el campos necesarios y lo crea. 
 */
router.post('/', async (req, res) => {
    const servicio = new Servicio(req.body);
    await servicio.save();
    res.json({
        status: 'Servicio Creado'
      });
});


/**
 * @async
 * Función para obtener servicios en específico mediante petición GET más el ID del objeto.
 */
router.get('/:id', async (req, res) => {
    const servicios = await Servicio.findById(req.params.id);
    res.json(servicios);
  });

module.exports = router;