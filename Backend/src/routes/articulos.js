const express = require('express');
const router = express.Router();

const Articulo = require('../models/Articulo');

/**
 * @async
 * Función para obtener todos los 
 * artículos mediante petición GET
 */
router.get('/', async (req, res) => {
    const articulos = await Articulo.find();
    res.json(articulos);
});

/**
 * @async
 * Función para crear artículos 
 * mediante petición POST, toma el 
 * campos necesarios y lo crea.
 */
router.post('/', async (req, res) => {
    const articulos = new Articulo(req.body);
    await articulos.save();
    res.json({
        status: 'Articulo Creado'
      });
});

/**
 * @async
 * Función para obtener artículo 
 * en específico mediante petición 
 * GET más el ID del objeto.
 */
router.get('/:id', async (req, res) => {
    const articulos = await Articulo.findById(req.params.id);
    res.json(articulos);
  });

/**
 * @async
 * Función para actualizar un artículo. 
 * Mediante petición PUT más el ID del artículo. 
 * Obtiene el artículo y lo actualiza 
 * según nueva información. 
 */
router.put('/:id', async (req, res) => {
    await Articulo.findByIdAndUpdate
    (req.params.id, req.body);
    res.json({
        status: 'Articulo Actualizado'
      });
});

/**
 * @async
 * Función para eliminar un artículo. 
 * Mediante petición DELETE más el ID del artículo. 
 * Obtiene el artículo y lo elimina. 
 */
router.delete('/:id', async (req, res) => {
    await Articulo.findByIdAndRemove
    (req.params.id);
    res.json({
      status: 'Articulo Eliminado'
    });
  });
 
module.exports = router;

