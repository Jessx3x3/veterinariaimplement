const express = require('express');
const router = express.Router();

const Comentario = require('../models/Comentario');

/**
 * @async
 * Función para obtener todos los comentarios mediante petición GET
 */
router.get('/', async (req, res) => {
    const comentarios = await Comentario.find();
    res.json(comentarios);
});

/**
 * @async
 * Función para crear comentarios mediante petición POST, toma el campos necesarios y lo crea. 
 */
router.post('/', async (req, res) => {
    const coment = new Comentario(req.body);
    await coment.save();
    res.json({
        status: 'Comentario Creado'
      });
});

/**
 * @async
 * Función para obtener comentario en específico mediante petición GET más el ID del objeto. 
 */
router.get('/:id', async (req, res) => {
    const coment = await Comentario.findById(req.params.id);
    res.json(coment);
  });

/**
 * @async
 * Función para eliminar un comentario. Mediante petición DELETE más el ID del comentario. Obtiene el comentario y lo elimina. 
 */
router.delete('/:id', async (req, res) => {
    await Comentario.findByIdAndRemove(req.params.id);
    res.json({
      status: 'Comentario Eliminado'
    });
  });

module.exports = router;