const express = require("express")
const users = express.Router()
const cors = require("cors")
const jwt = require("jsonwebtoken")
const bcrypt = require("bcrypt")

const User = require("../models/User")
users.use(cors())

process.env.SECRET_KEY = 'secret'

var bol = false;

users.get('/', async (req, res) => {
    const user = await User.find();
    res.json(user);
});

users.delete('/:id', async (req, res) => {
    await User.findByIdAndRemove(req.params.id);
    res.json({
        status: 'Usuario Eliminado'
    });
});

users.get('/:id', async (req, res) => {
    const usuario = await User.findById(req.params.id);
    res.json(usuario);
});

users.post("/register", (req, res) => {

    /** 
    * @param{date} new Date
    * Registra al usuario nuevo. A través de POST con los siguientes parámetros:
    * 
    * @param{string} first name
    * @param{string} last name
    * @param{string} email
    * @param{string} password
    * @param{date}   create
    * 
    *Verifica que el usuario exísta, si exíste 'error'. Sino existe, encripta password y crea usuario   
    */

    const today = new Date()
    const userData = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: req.body.password,
        created: today
    }

    User.findOne({
        email: req.body.email
    })
        .then(user => {
            if (!user) {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    userData.password = hash
                    User.create(userData)
                        .then(user => {
                            res.json({ status: user.email + ' registered' })
                        })
                        .catch(err => {
                            res.send('error: ' + err)
                        })
                })
            } else {
                res.json({ error: 'User already exists' })
            }
        })
        .catch(err => {
            res.send('error: ' + err)
        })
})

users.post('/login', (req, res) => {

    /**
     * Función mediante POST, ubica el mail y verifica si exíste. Si exíste, verifica que exísta la contraseña luego de quitar la encriptación.
     */

    User.findOne({
        email: req.body.email
    })
        .then(user => {
            if (user) {
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    const payload = {
                        _id: user._id,
                        first_name: user.first_name,
                        last_name: user.last_name,
                        email: user.email
                    }
                    let token = jwt.sign(payload, process.env.SECRET_KEY, {
                        expiresIn: 1440
                    })
                    //res.send(token)
                    bol = true,
                    res.send(bol)
                } else {
                    res.json({ error: 'User does not exist' })
                }
            } else {
                res.json({ error: 'User does not exist' })
            }
        })
        .catch(err => {
            res.send('error: ' + err)
        })
})

/**
 * @async
 * Función para obtener usuarios en específico mediante petición GET más el ID del objeto.
 */
users.get('/:id', async (req, res) => {
    const usuarios = await User.findById(req.params.id);
    res.json(usuarios);
  });


module.exports = users