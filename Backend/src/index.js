const express = require('express');
const morgan = require('morgan');
const formidable = require('express-form-data');
const app = express();

var cors = require('cors');
var bodyParser = require('body-parser');

const mongoose = require('mongoose');

//Conexión con base de datos a dirección donde serán creados los schemas y collections
mongoose.connect('mongodb://localhost/veterinaria', {
    useNewUrlParser: true 
})
    .then(db => console.log('DB is connected'))
    .catch(err => console.log(err));

//Configuración: acepta cualquier número de puerto disponible o 3000
app.set('port', process.env.PORT || 3000);

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

//Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(formidable.parse({ keepExtensions: true}));

//Routes
app.use('/comentarios', require('./routes/comentarios'));
app.use('/articulos', require('./routes/articulos'));
app.use('/users', require('./routes/users'));
app.use('/reservas', require('./routes/reservaLista'));
app.use('/servicios', require('./routes/servicios'));

//Filas estáticas
app.use(express.static(__dirname + '/public'));

//Notificación sobre qué puerto está escuchando
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
  });

module.exports = app;