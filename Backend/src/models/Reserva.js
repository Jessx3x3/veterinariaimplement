const mongoose = require('mongoose');
const { Schema } = mongoose;

//Crear schema de reserva de hora con los siguiente atributos:
const Reserva = new Schema({
    /**
     * Crear un reserva.
     * @param {string} email
     * @param {string} nombre
     * @param {string} mascota
     * @param {string} servicio
     * @param {date} fechaSolicitud
     * @param {number} seleccionaHora
     * @param {date} crearSolicitud
     */
    email: {
        type: String,
        Required: 'Ingresa tu email'
    },
    nombre: {
        type: String,
        Required: 'Ingresa tu nombre'
    },
    mascota: {
        type: String,
        Required: 'Ingresa tu mascota que necesita atención'
    },
    servicio: {
        type: String,
        Required: 'Ingresa el servicio que necesitas'
    },
    fechaSolicitud: {
        type: Date,
        Required: 'Ingresa la fecha de solicitud',
        min: '2020-09-08',
        max: '2020-12-23'
    },
    seleccionaHora: {
        type: Number
    },
    crearSolicitud: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Reserva', Reserva);
