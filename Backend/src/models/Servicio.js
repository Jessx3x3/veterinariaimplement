const mongoose = require('mongoose');
const { Schema } = mongoose;

//Crear schema de servicio con los siguiente atributos:
const Servicio = new Schema({
    /**
     * Crear un servicio.
     * @param {string} imgUrl
     * @param {string} titulo
     * @param {string} descripcion
     */
    imgUrl: String,
    titulo: String,
    descripcion: String
});
module.exports = mongoose.model('Servicio', Servicio);