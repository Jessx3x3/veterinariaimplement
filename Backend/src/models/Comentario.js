const mongoose = require('mongoose');
const { Schema } = mongoose;

//Nuevo schema Comentario. Con los siguientes atributos: 
const Comentario = new Schema({
    /**
     * Crear un comentario.
     * @param {string} email
     * @param {string} comentario
     */
    email: String,
    comentario: String
});
module.exports = mongoose.model('Comentario', Comentario);