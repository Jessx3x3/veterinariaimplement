const mongoose = require('mongoose');
const { Schema } = mongoose;

//Nuevo schema Artículo. Con los siguientes atributos: 
const Articulo = new Schema({
    /**
     * Crear un artículo.
     * @param {string} titulo
     * @param {string} subtitulo
     * @param {string} imgUrl
     * @param {string} contenido
     */
    titulo: String,
    subtitulo: String,
    imgUrl: String,
    contenido: String
});
module.exports = mongoose.model('Articulo', Articulo);




