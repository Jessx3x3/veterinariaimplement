const mongoose = require("mongoose")
const Schema = mongoose.Schema

//Crear schema de usuario con los siguiente atributos:
const UserSchema = new Schema({
    /**
     * Crear un usuario.
     * @param {string} first_name
     * @param {string} last_name
     * @param {string} email
     * @param {string} passsword
     * @param {date} date
     */
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('users', UserSchema) //module.exports = User = mongoose