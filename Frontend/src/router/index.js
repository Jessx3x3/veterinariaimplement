import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/HomePage.vue')
  },
  {
    path: '/blog',
    name: 'Blog',
    component: () => import('../views/Blog.vue')
  },
  {
    path: '/admin',
    name: 'Admin',
    component: () => import('../views/LoginAdmin.vue')
  },
  {
    path: '/index',
    name: 'Index',
    component: () => import('../views/IndexAdmin.vue')
  },
  {
    path: '/horasReservadas',
    name: 'Horas',
    component: () => import('../views/HorasMedicas.vue')
  },
  {
    path: '/gestionUsuarios',
    name: 'Usuarios',
    component: () => import('../views/GestionUsuarios.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
