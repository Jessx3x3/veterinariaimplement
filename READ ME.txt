Iniciar servidor de servicios de Mongodb = Mongod

Backend - 

npm install
npm run dev
dirección: http://localhost:3000/

Frontend - 

npm install
npm run serve
dirección: http://localhost:8080/


Base de datos - 

instalar https://www.mongodb.com/try/download/database-tools
Para Importar y Exportar Base de datos necesaria para leer el único usuario que posee (No contiene la opción registrar, no es necesario)
Inicial consola desde carpeta
Comandos: 

mongoimport --db veterinaria --collection reservas --file C:/Users/{{ Nombre Usuario }}/Downloads/Veterinaria/database/reservas.json
mongoimport --db veterinaria --collection comentarios --file C:/Users/{{ Nombre Usuario }}/Downloads/Veterinaria/database/comentarios.json
mongoimport --db veterinaria --collection articulos --file C:/Users/{{ Nombre Usuario }}/Downloads/Veterinaria/database/articulos.json
mongoimport --db veterinaria --collection users --file C:/Users/{{ Nombre Usuario }}/Downloads/Veterinaria/database/users.json
mongoimport --db veterinaria --collection servicios --file C:/Users/{{ Nombre Usuario }}/Downloads/Veterinaria/database/servicios.json


Usuario registrado: 
millarayarias@vet.cl

Password 
veterinaria
